const mongoose = require("mongoose")
const Schema = mongoose.Schema

const bookingSchema = new Schema({
	inDate:{
		type:Date,
		required:true
	},
	outDate:{
		type:Date,
		required:true
	},
	hutId:{
		type:String,
		required:true
	},
	userId:{
		type:String,
		required:true
	},
	statusId:{
		type:String,
		required:true
	},
	total:{
		type:Number,
		required:true
	},
	isDeleted:{
		type:Boolean
	}
},{
	timestamps:true
})

 // export the model as a module
 module.exports = mongoose.model('Booking', bookingSchema);