import React,{useState} from 'react';
import './App.css';
import { BrowserRouter, Switch, Route, Redirect} from "react-router-dom";
import ApolloClient from "apollo-boost";
import {ApolloProvider} from "react-apollo";

import { Layout, Menu, Icon } from 'antd';
import SideBar from './components/layouts/Layout';
import Main from './components/layouts/Main';
import Dashboard from "./components/Dashboard";
import Hut from "./components/Hut";
import User from './components/User';
import Booking from './components/Booking';
import UpdateHut from './components/UpdateHut';
import UpdateUser from './components/UpdateUser';

import NavBar from "./components/userview/NavBar";
import Login from "./components/userview/Login";
import Landing from "./components/userview/Landing";
import LandingPage from "./components/userview/LandingPage";
import BookingPage from "./components/userview/BookingPage";
import NipaHut from "./components/userview/NipaHut";
import Transaction from "./components/userview/Transaction";


const { Content, Footer, Sider, Header } = Layout;


const client = new ApolloClient({ uri:"http://localhost:4010/somewheretostay"});


function App() {

  const [username,setUsername] = useState(localStorage.getItem("username"));
  const [role,setRole] = useState(localStorage.getItem("role"));

  const localStorageChecker = () =>{
    
    if(role === "admin"){
            return(

              <Layout>
                <SideBar username={username}/>
                  <Layout>
                    <Content style={{ margin: '24px 16px 0' }}>
                      <Main>
                          <ApolloProvider client={client}>
                              <BrowserRouter>
                                <Switch>
                                  <Route exact path="/admin" component={Dashboard} />
                                  <Route exact path="/admin/huts" component={Hut} />
                                  <Route exact path="/admin/hut/update/:id" component={UpdateHut} />
                                  <Route exact path="/admin/users" component={User}/>
                                  <Route exact path="/admin/user/update/:id" component={UpdateUser} />
                                  <Route exact path="/admin/bookings" component={Booking} />
                                  <Route path="/login" render={loggedUser} />
                                  <Route path="/logout" component={Logout} />
                                  <Route exact path="/" component={Landing} />
                              
                                </Switch>
                              </BrowserRouter>
                           </ApolloProvider>
                      </Main>
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>SomeWhereToStay ©2019 Created by JREX</Footer>
                  </Layout>
                </Layout>


              );

    }
    else{
      return(

        <Layout>
        
              <Layout>
                    <Content style={{ margin: '24px 16px 0' }}>
                    <LandingPage username={username} >
                      <Main>
                          <ApolloProvider client={client}>
                              <BrowserRouter>
                                <Switch>
                                  <Route exact path="/" component={NipaHut} />
                                  <Route exact path="/nipa-huts" component={NipaHut} />
                                  <Route exact path="/transaction" component={Transaction} />
                                  <Route path="/login" render={loggedUser} />
                                  <Route path="/logout" component={Logout} />
                                </Switch>
                              </BrowserRouter>
                           </ApolloProvider>

                      </Main>
                    </LandingPage>
                      
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>SomeWhereToStay ©2019 Created by JREX</Footer>
                </Layout>
        </Layout>
        );
    }
    

  }

  const updateSession = () =>{
    localStorage.clear();
    setUsername(localStorage.getItem("username"));
    setRole(localStorage.getItem("role"));
    // localStorageChecker();
  }

  const Logout = () =>{
    updateSession();
    return(<Redirect to="/" />);
  }

   const loggedUser = props =>{
    //retain operator
    return( <Login {...props} updateSession={updateSession} localStorageChecker= {localStorageChecker}/>);
  }


  return (
    <div>
        {localStorageChecker()}
  </div>
  );
}

export default App;
