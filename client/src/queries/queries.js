import {gql} from "apollo-boost";


const getHutsQuery = gql`

{
  getHuts{
    id
    name
    description
    price
    hutStatusId
    categoryId
    image
    category{
      name
    }
    hutStatus{
      name
    }
  }
}

`;

const getUsersQuery = gql`
{
    getUsers{
      id
      firstname
      lastname
      address
      telNo
      email
      username
      password
      roleId
      image
      role{
        name
      }
    }
  }
`;

const getCategoriesQuery = gql `
{
  getCategories{
    id
    name
  }
}
`;

const getHutStatusesQuery = gql`
{
	getHutStatuses{
		id
		name
	}
}
`;

const getBookingsQuery = gql`
{
    getBookings{
      id
      inDate
      outDate
      hutId
      userId
      statusId
      total
      hut{
        id
        name
        price
      }
      user{
        id
        firstname
        lastname
        email
      }
      status{
        id
        name
      }
    }
  }
`;

const getRolesQuery =gql`
{
    getRoles{
      id 
      name
    }
  }
`;
const getBookingStatusesQuery = gql`
{
    getBookingStatuses{
      id
      name
    }
  }
`;

const getHutQuery = gql`
query($id:ID!){
  getHut(id:$id){
    id
    name
    description
    price
    categoryId
    hutStatusId
    image
    category{
      name
    }
    hutStatus{
      name
    }
  }
}
`;
const getUserQuery = gql`
query($id:ID!){
    getUser(id:$id){
      id
      firstname
      lastname
      address
      telNo
      email
      username
      password
      roleId
      image
      role{
        name
      }
    }
  }
`;

const getBookingQuery = gql`
query($userId:String!){
    getBooking(userId:$userId){
      id
      inDate
      outDate
      hutId
      userId
      total
      hut{
        name
      }
      user{
        firstname
        lastname
      }
      status{
        name
      }
    }
  }
`;


export {getHutsQuery,
        getCategoriesQuery,
        getHutStatusesQuery,
        getUsersQuery,
        getRolesQuery,
        getBookingStatusesQuery,
        getBookingsQuery,
        getHutQuery,
        getUserQuery,
        getBookingQuery

      };