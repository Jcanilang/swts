import {gql} from "apollo-boost";

const createHutMutation = gql`
mutation(
	$name:String! 
  	$description:String! 
  	$price: Float!
  	$hutStatusId:String!  
  	$categoryId:String!
    $image:String! 
){
  createHut(
  name: $name
  description: $description
  price: $price
  hutStatusId: $hutStatusId 
  categoryId: $categoryId
  image:$image
  ){
  name 
  description
  price
  hutStatusId 
  categoryId
  image
  isDeleted
  }
}
`;

const createBookingMutation =gql`
mutation(
  $inDate:String
  $outDate:String
  $hutId:String!
  $userId:String!
  $statusId:String!
  $total:Float!
){
  createBooking(
    inDate:$inDate
    outDate:$outDate
    hutId:$hutId
    userId:$userId
    statusId:$statusId
    total:$total
  ){
    id
    inDate
    outDate
    userId
    statusId
    total
    isDeleted
   
  }
}
`;





const updateHutMutation = gql`
mutation(
    $id:ID!
    $name:String! 
  	$description:String! 
  	$price: Float!
  	$hutStatusId:String!  
  	$categoryId:String!
    $image:String!
){
    updateHut(
        id:$id
        name:$name 
        description:$description 
        price: $price
        hutStatusId:$hutStatusId  
        categoryId:$categoryId
        image:$image
    ){
        id
        name
        description
        hutStatusId
        categoryId
        image
        hutStatus{
          name
        }
        category{
          name
        }
    }
}
`;


const createUserMutation = gql`
mutation(
  $firstname:String!
  $lastname:String!
  $address:String!
  $telNo:String!
  $email:String!
  $username:String!
  $password:String!
  $roleId:String!\
  $image:String!
){
  createUser(
    firstname:$firstname
    lastname:$lastname
    address:$address
    telNo:$telNo
    email:$email
    username:$username
    password:$password
    roleId:$roleId
    image:$image
  ){
    id
    firstname
    lastname
    address
    telNo
    email
    username
    password
    roleId
    image
    role{
      name
    }
  }
}
`;
const updateUserMutation = gql`
mutation(
  $id:ID!
  $firstname:String!
  $lastname:String!
  $address:String!
  $telNo:String!
  $email:String!
  $username:String!
  $password:String!
  $roleId:String!
  $image:String!
){
  updateUser(
    id:$id
    firstname:$firstname
    lastname:$lastname
    address:$address
    telNo:$telNo
    email:$email
    username:$username
    password:$password
    roleId:$roleId
    image:$image
  ){
    id
    firstname
    lastname
    address
    telNo
    email
    username
    password
    roleId
    image
    role{
      name
    }
  }
}

`;

const deleteHutMutation = gql`
mutation($id:String!){
  deleteHut(
		id:$id
		)
}

`;

const updateRequestMutation = gql`
mutation(
  $id:String! 
  $statusId:String!
  ){
  updateRequest(
    id:$id
    statusId: $statusId
    )
}

`;

const deleteUserMutation = gql`
mutation($id:String!){
  deleteUser(
		id:$id
		)
}

`;

const deleteBookingMutation = gql`
mutation($id:String!){
  deleteBooking(
    id:$id
    )
}

`;

const loginMutation = gql `
  
mutation($username:String! $password:String!){
  logInUser(username:$username password:$password){
    id
    firstname
    lastname
    address
    telNo
    email
    username
    password
    roleId
    role{
      name
    }
    
  }
}
  
`;


export {createHutMutation, 
  deleteHutMutation,
  updateHutMutation,
  createUserMutation,
  updateUserMutation,
  deleteUserMutation,
  loginMutation,
  updateRequestMutation,
  deleteBookingMutation,
  createBookingMutation
};