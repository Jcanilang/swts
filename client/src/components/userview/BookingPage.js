import React, {useState, Component} from "react";
import {Row,Col,Card,Icon,Carousel, Form, Button, Input, DatePicker, Modal} from 'antd';
import moment from "moment";
import {graphql} from "react-apollo";
import {flowRight as compose} from "lodash";

import "../../App.css";

import {getHutQuery,getBookingStatusesQuery, getBookingsQuery} from "../../queries/queries";
import {createBookingMutation} from "../../queries/mutations";

const BookingPage = (props)=> {


	let hutData = props.getHutQuery.getHut? props.getHutQuery.getHut :{};
	let bookStatsData = props.getBookingStatusesQuery.getBookingStatuses? props.getBookingStatusesQuery.getBookingStatuses :[];

	let bookingsData = props.getBookingsQuery.getBookings ? props.getBookingsQuery.getBookings:[];

// date picker hooks
 	const [startValue,setStartValue] = useState();
 	const [endValue,setEndValue] = useState();
 	const [endOpen, setEndOpen] = useState(false);


 	const hutId = hutData.id;
 	const price = (moment(endValue).diff(moment(startValue), "days")+ 1)*hutData.price;
 	let statusId = "";
	const userId = localStorage.getItem("id");


 	bookStatsData.map(bookStat =>{
 		if(bookStat.name == "Pending"){
 			statusId = bookStat.id;
 		}
 	})


const addBooking = e =>{

		e.preventDefault();

		let newBooking = {
			inDate:startValue,
			outDate: endValue,
			userId: userId,
			hutId: hutId,
			statusId: statusId,
			total:price
    	}
    
    let secondsToGo = 2;
    const modal = Modal.success({
        title: 'Request sent!',
        content: `This modal will be destroyed after ${secondsToGo} second.`,
    });
    const timer = setInterval(() => {
        secondsToGo -= 1;
        modal.update({
        content: `This modal will be destroyed after ${secondsToGo} second.`,
        });
    }, 1000);
    setTimeout(() => {
        clearInterval(timer);
        props.createBookingMutation({
          variables:newBooking
        }).then(()=>{ window.location.href = "/"});
        modal.destroy();

    }, secondsToGo * 1000);

        // console.log("success");

	}

	const [startDates,setStartDates] = useState(0);

	const disabledStartDate = (current)=>{

		let blockDateStart = [props.getBookingsQuery.getBookings.map(books=>{
			if(books.status.name === "Approved" && books.hutId === hutId){
					return (moment(books.inDate).format("YYYY-MM-DD"));
			}
		})]

		let blockDateEnd = [props.getBookingsQuery.getBookings.map(books=>{
			if(books.status.name === "Approved" && books.hutId === hutId){
					return(moment(books.outDate).format("YYYY-MM-DD"));
			}
		})]

		for(let i = 0; i < blockDateStart.length; i++){
			setStartDates(i);
			if(moment(current).isBetween(blockDateStart[0][i], blockDateEnd[0][i], null,'[]')){
				return true;
			}
		}
		return (!current || current.isBefore(moment().subtract(1, 'days')))

	}


const [endDates,setEndDates] = useState(0);

	const disabledEndDate = (current)=>{

		let blockDateStart = [props.getBookingsQuery.getBookings.map(books=>{
			if(books.status.name === "Approved" && books.hutId === hutId){
					return (moment(books.inDate).format("YYYY-MM-DD"));
			}
		})]

		let blockDateEnd = [props.getBookingsQuery.getBookings.map(books=>{
			if(books.status.name === "Approved" && books.hutId === hutId){
					return(moment(books.outDate).format("YYYY-MM-DD"));
			}
		})]

		for(let i = 0; i < blockDateStart.length; i++){
			setEndDates(i);
			if(moment(current).isBetween(blockDateStart[0][i], blockDateEnd[0][i], null,'[]')){
				return true;
			}
		}
		return (!current || current.isBefore(moment(startValue)))
		

	}


	const onStartChange = (field,value) =>{
		// console.log(value)
		setStartValue(value);
		setEndValue(value);

	}

	const onEndChange = (field,value) => {
    	setEndValue(value);
    	// console.log(endValue)
  	}

  	const handleStartOpenChange = open =>{
  		if (!open) {
      		setEndOpen(true);
    	}
  	}

  	const handleEndOpenChange = open =>{
  		if (!open) {
      		setEndOpen(open);

    	}
  	}

    return(
		<div>

			<Card bordered={false}>
						    	<Form onSubmit={addBooking} layout="vertical">
					               <Form.Item label="Check In">
							          <DatePicker
								          // disabledDate={disabledStartDate}
								          // d => !d || d.isBefore(moment().subtract(1, 'days'))
								          disabledDate={disabledStartDate}
								          showTime
								          format="YYYY-MM-DD"
								          value={moment(startValue)}
								          placeholder="Start"
								          onChange={onStartChange}
								          onOpenChange={handleStartOpenChange}
								          name="start"
								        />
							       </Form.Item>
							            
							       <Form.Item label="Check Out">
							          <DatePicker
								          // disabledDate={disabledEndDate}
								          // d => !d || d.isBefore(moment(startValue))
								          disabledDate={disabledEndDate}
								          showTime
								          format="YYYY-MM-DD"
								          value={moment(endValue)}
								          placeholder="End"
								          onChange={onEndChange}
								          open={endOpen}
								          onOpenChange={handleEndOpenChange}
								          name="end"
								        />
							        </Form.Item>

							        <Form.Item label="Hut">
							          <Input value={hutData.name} />
							        </Form.Item>
							            
							        <Form.Item label="Price">
							          <Input type="number" value={price} />
							        </Form.Item>
							         
							       <Form.Item>
							          <Button type="primary"  htmlType="submit" block>
							            Submit
							          </Button>
							       </Form.Item>
				</Form>
			</Card>
        </div>
    );
};

export default compose(
	graphql(createBookingMutation,{name:"createBookingMutation"}),
	graphql(getBookingStatusesQuery,{name:"getBookingStatusesQuery"}),
	graphql(getBookingsQuery,{name:"getBookingsQuery"}),
    graphql(getHutQuery,{name:"getHutQuery",
    	options:props => {
    		return{
    			variables:{
    				id:props.id
    			}
    		}
    	}

	})

	)(BookingPage);