import React,{useState, useEffect} from "react";
import "../../App.css";
import {Link} from "react-router-dom";
import {graphql} from "react-apollo";
import {toBase64, nodeServer} from "../../function.js"
import {flowRight as compose} from "lodash";
import {
    Icon,
    Row,
    Col,
    Button,
    Divider,
    Modal,
    Card,
    Drawer,
    Tag,
   	Typography,
   	message
  } from 'antd';

  import {getHutsQuery,getBookingQuery} from "../../queries/queries";
  import BookingPage from "./BookingPage"

const { Text, Paragraph,Title } = Typography;

const NipaHut = (props)=> {
	// console.log(props)
      // const mongoose = require('mongoose');
	const dataHuts = props.getHutsQuery;

	const hutsData = dataHuts.getHuts ? dataHuts.getHuts:[];

	const dataBookings = props.getBookingQuery;
	const bookingsData = dataBookings.getBooking ? dataBookings.getBooking:[];

	const [Id, setHutId] = useState();

	const [visible, setVisible] = useState(false);
 	const userId = localStorage.getItem("id")

	 const showDrawer = e => {
	 	let hutID = e.target.id;

	 	// console.log(hutID)
	  	if(!userId) {
	  		 message.warning('You must login first!!')
	  		 .then(()=>{
	  		 	window.location.href="/login#huts";
	  		 })
	  	}
	  	else{
	  		// console.log(bookingsData)
	  		//check if the data is empty or not
	  		if(bookingsData.length > 0){
	  			// setVisible(true)
	  			// setHutId(hutID)
	  					bookingsData.map(row=>{
	  						if(row.userId === userId && row.status.name == "Approved"){
	  									message.warning('Please complete you previous transaction!')
					  					setVisible(false)
	  						}
	  						else{
	  							if(row.hutId === hutID){
					  				// console.log("meron na");
					  				if(row.status.name === "Pending" || row.status.name === "Approved"){
					  					message.warning('You have already sent a request for this item!')
					  					setVisible(false)
					  				}else{
					  					setVisible(true)
										setHutId(hutID)
					  				}
					  			}
					  			else{
					  				setVisible(true)
									setHutId(hutID)
					  			}
	  						}
					  			
			  			})
	  		}else{
	  			setVisible(true)
	  			setHutId(hutID)
	  		}  	
	  	}	      
	  };

	  const onClose = () => {
	      setVisible(false)
	  };
	  
    return(
        <div>
     
        <Title level={2}>HUTS</Title>
        <Row gutter={8,8}>
      <Col span={24}>
      {hutsData.map(hut=>{
      	// console.log(hut.id);
      	return(
      		<div>
		        <Card style={{ marginTop: 16 }} hoverable title={hut.name} bordered={false} extra={<Tag color="green">{hut.hutStatus.name}</Tag>}>
		        	<Row gutter={8}>
		        	<Col span={8}>
		        		<div className="img-hover-zoom">
		        		<img className="logo-img" src={nodeServer()+hut.image}  alt="nipa-main-image" />
		        		</div>
		        	</Col>
		        	<Col span={12}>
		        	<div className="hut-descriptions">
		        		<Paragraph>
			          	{hut.description}
			          </Paragraph>
			          <p>Category: <span>{hut.category.name}</span></p>
			          <p>Price: <span className="span">{hut.price}</span>  <Text disabled>Per day</Text></p>
			
			          <Button onClick={showDrawer} id={hut.id} type="primary" size="large">Book</Button>
			          </div>
		        	</Col>
		        	</Row>
		        </Card>

		        <Drawer
		          title="Booking Details"
		          width={350}
		          height ={200}
		          onClose={onClose}
		          visible={visible}
		          bodyStyle={{ paddingBottom: 80 }}
		        >

		        <BookingPage id={Id} />
		        </Drawer>
      		</div>

      		);
      })}
      
      </Col>
    </Row>
        </div>
    );
};


export default compose(
	graphql(getHutsQuery,{name:"getHutsQuery"}),
	  graphql(getBookingQuery,{name:"getBookingQuery",
	  	options:props => {
	    		return{
	    			variables:{
	    				userId:localStorage.getItem("id")
	    			}
	    		}
	    	}

		})
	)(NipaHut);