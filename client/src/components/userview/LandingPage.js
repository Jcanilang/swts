import React from "react";
import "../../App.css";


import { DatePicker,Row, Col, Card,Tabs, Divider } from 'antd';
import NavBar from "./NavBar";
import Main from "../layouts/Main";

const { RangePicker } = DatePicker;
const { TabPane } = Tabs;


const Landing = (props)=> {

  const callback = (key) =>{
  console.log(key);
}

    return (
      <div id="landing">
        <section>
          <header>
            <img src="./images/logo.png" width="150" alt="logo" />
            <div>
              <NavBar username={props.username} />
            </div>
          </header>
        </section>

        <section id="main">
          <div className="main-text">
            <h2>Hi! {props.username?props.username:"Guest"} </h2>
            <span className="span">swts.</span> 
            <p>where we reinvent</p>
            <p>the meaning of selfcare</p>
          </div>

          <img className="logo-img" src="./images/logo-img.png" width="500" alt="nipa-main-image" />
        </section>
        <Divider />
        <section id="huts">
          <div className="services-text">
            {props.children}
          </div>
        </section>
      </div>
    );

}

export default Landing;