import React, {useState} from "react";
import {Row,Col,Card,Icon,Carousel, Form, Button, Input, DatePicker} from 'antd';


import "../../App.css";
// const { tz} = moment;


const Landing = (props)=> {

 const [checkIn, setCheckIn] = useState();
 const [checkOut, setCheckOut] = useState();

 const [endOpen, setEndOpen] = useState(false);


	 // const disabledStartDate = startValue => {    
	 //    setEndValue(false)
	 //    if (!startValue || !endValue) {
	 //      return false;
	 //    }
	 //    return startValue.valueOf() > endValue.valueOf();
	 //  };

	 //  const disabledEndDate = endValue => {
	 //    setStartValue(false)
	 //    if (!endValue || !startValue) {
	 //      return false;
	 //    }
	 //    return endValue.valueOf() <= startValue.valueOf();
	 //  };

 //  const onChange = (field, value) => {
 //    setState({
 //      [field]: value,
 //    });
 //  };

	  // onStartChange = e => {
	  //   this.onChange('startValue', value);
	  // };

 //  onEndChange = value => {
 //    this.onChange('endValue', value);
 //  };

 //  handleStartOpenChange = open => {
 //    if (!open) {
 //      this.setState({ endOpen: true });
 //    }
 //  };

 //  handleEndOpenChange = open => {
 //    this.setState({ endOpen: open });
 //  };

 	const  checkInChangeHandler = (date,dateString) =>{
 		// setCheckIn(moment(dateString).tz("Asia/Manila"));
 		// console.log(checkIn);
 	}

 	const  checkOutChangeHandler = (date,dateString) =>{
 		// setCheckOut(moment(dateString).tz("Asia/Manila"));
 		// console.log(checkOut);
 	}

 	const priceChangeHandler = e =>{}

 	const hutChangeHandler = e =>{}


    return(
		<div className="first-slide" >
			<div className="Booking-section">
				<Row gutter={16,16}>
					<Col md={9}>
						 <Card bordered={false}>
						    	<Form layout="vertical">
					            <Row gutter={16}>
					              <Col span={12}>
					                <Form.Item label="Check In">
							          <DatePicker
							           	
								          showTime
								          format="YYYY-MM-DD HH:mm:ss"
								          value={checkIn}
								          placeholder="Start"
								          onChange={checkInChangeHandler}
								          

							           />
							        </Form.Item>
							              </Col>
							              <Col span={12}>
							         <Form.Item label="Check Out">
							          <DatePicker
							           
								          showTime
								          format="YYYY-MM-DD HH:mm:ss"
								          value={checkOut}
								          placeholder="End"
								          onChange={checkOutChangeHandler}
								          

							           />
							        </Form.Item>
							              </Col>
							            </Row>
							            <Row gutter={16}>
							              <Col span={12}>
							                 <Form.Item label="Hut">
							          <Input  onChange={hutChangeHandler} />
							        </Form.Item>
							              </Col>
							              <Col span={12}>
							                <Form.Item label="Price">
							          <Input type="number"  onChange={priceChangeHandler} />
							        </Form.Item>
							              </Col>
							            </Row>
							            <Row gutter={16}>
							              <Col span={24}>
							                <Form.Item label="Address">
							                 <Input.TextArea rows={4}  />
							                </Form.Item>
							              </Col>
							            </Row>
							            <Form.Item>
							          <Button type="primary"  htmlType="submit" block>
							            Register
							          </Button>
							        </Form.Item>
					          </Form>
						    </Card>
	
					</Col>
					<Col md={8}>
					<img className="logo-img" src="./images/logo-img.png" width="500" alt="nipa-main-image" />
					</Col>
				</Row>
			</div>
        </div>
    );
};

export default Landing;