import React from "react";
import {Row,Col,Card,Icon} from 'antd';
import "../App.css";
import {graphql} from "react-apollo";
import {flowRight as compose} from "lodash";


import {getUsersQuery,getHutsQuery, getBookingsQuery} from "../queries/queries";
const Dashboard = (props)=> {
    return(
        <div>
        <Row gutter={[8,8]}>
		    <Col xs={{ span: 5, offset: 1 }} lg={{ span: 6, offset: 2 }}>
		    	<a href="/admin/users">
		      <Card id="user"  bordered={false} >
			      <Icon type="user" />
			      <h2>User</h2>
			      <h1></h1>
			    </Card>
			    </a>
		    </Col>

		    <Col xs={{ span: 11, offset: 1 }} lg={{ span: 6, offset: 2 }}>
		    <a href="/admin/huts">
		      <Card id="hut"  bordered={false} >
			      <Icon type="home" />
			      <h2>Hut</h2>
			    </Card>
			    </a>
		    </Col>
		    <Col xs={{ span: 5, offset: 1 }} lg={{ span: 6, offset: 2 }}>
		    <a href="/admin/bookings">
		       <Card id="booking"  bordered={false}>
			      <Icon type="book" />
			      <h2>Booking</h2>
			    </Card>
			    </a>
		    </Col>
		 </Row>
        </div>
    );
};

export default compose(
	graphql(getUsersQuery,{name:"getUsersQuery"}),
	graphql(getHutsQuery,{name:"getHutsQuery"}),
	graphql(getBookingsQuery,{name:"getBookingsQuery"})
	)(Dashboard);

