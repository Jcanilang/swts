import React,{useState, useEffect} from "react";
import "../App.css";
import {Link} from "react-router-dom";
import {graphql} from "react-apollo";
import {flowRight as compose} from "lodash";
import {toBase64, nodeServer} from "../function.js";
import bcrypt from "bcryptjs";
import {
    Form,
    Input,
    Icon,
    Cascader,
    Select,
    Row,
    Col,
    Checkbox,
    Button,
    Table, 
    Divider,
    Modal,
    message,
    Upload
  } from 'antd';

  import {getUserQuery,getRolesQuery} from "../queries/queries";
  import { updateUserMutation } from "../queries/mutations"; 



  const { Option } = Select;
   // const { confirm } = Modal;

const UpdateUser = (props)=> {

    // const mongoose = require('mongoose');
	let user = props.getUserQuery.getUser? props.getUserQuery.getUser :{};

	const dataRoles = props.getRolesQuery;
	const rolesData = dataRoles.getRoles ? dataRoles.getRoles:[];

	// console.log(UsersData);
	// hooks
	const [id, setId] = useState("");
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [address, setAddress] = useState("");
    const [telno, setTelno] = useState("");
    const [email,setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [roleId, setRoleId] = useState("");

	useEffect(()=>{
		console.log(id);
        console.log(firstname);
        console.log(lastname);
		console.log(address);
		console.log(telno);
		console.log(email);
        console.log(username);
        console.log(password);
        console.log(roleId);
	});

	// console.log(hut);

	if(!props.getUserQuery.loading){
		const setDefaultUser = ()=>{
			setId(user.id);
			setFirstname(user.firstname);
			setLastname(user.lastname);
			setTelno(user.telNo);
			setAddress(user.address);
			setEmail(user.email);
			setUsername(user.username);
			setPassword(user.password);
			setRoleId(user.roleId);
            // setImageUrl(user.image);
	
		}
		if(id === ""){
			setDefaultUser();
		}
	}

	const firstnameChangeHandler = e =>{
		setFirstname(e.target.value);
    }
    const lastnameChangeHandler = e =>{
		setLastname(e.target.value);
    }
    const telnoChangeHandler = e =>{
		setTelno(e.target.value);
	}

	const addressChangeHandler = e =>{
		setAddress(e.target.value);
	}

	const emailChangeHandler = e =>{
		setEmail(e.target.value);
	}

	const usernameChangeHandler = e =>{
		setUsername(e.target.value);
	}

	const passwordChangeHandler = e =>{
        setPassword(e.target.value);
        // console.log(e)
    }
    const roleIdChangeHandler = e =>{
        setRoleId(e);
        // console.log(e)
	}

	


	const updateUser = e =>{

		e.preventDefault();
    if(imageUrl == null || firstname == "" || lastname== "" || address== "" || telno== "" || email== "" || username== "" || password== "" || roleId== ""){
      // imagePath = nodeServer()+"/images/b857ab90-1b48-11ea-8070-a3e900f687b3.png";
      message.error("Please fill up all the field!!");
    }
    else{

    let saltRounds = 5;
    let pass = "";
        if(password === user.password){
            // setPassword(e.target.value);
            pass=password;
        }else{
           pass = bcrypt.hashSync(password,saltRounds);
        }

		let updateUserValue = {   
		id:user.id,
        firstname:firstname,
        lastname:lastname,
		    address:address,
		    telNo:telno,
	       email:email,
        username:username,
        password:pass,
        roleId:roleId,
        image: imageUrl
		}
		let secondsToGo = 1;
	    const modal = Modal.success({
	        title: 'Update successfully',
	        content: `This modal will be destroyed after ${secondsToGo} second.`,
          render: window.location.href ="/admin/users"
	    });
	    const timer = setInterval(() => {
	        secondsToGo -= 1;
	        modal.update({
	        content: `This modal will be destroyed after ${secondsToGo} second.`,
	        });
            
            
	    }, 1000)
	    setTimeout(() => {
	        clearInterval(timer);

	        modal.destroy();
	        

	    }, secondsToGo * 1000)

        props.updateUserMutation({
                variables:updateUserValue
                
        })
            
        console.log("success");
        
        setFirstname("")
        setLastname("")
  		  setAddress("")
  		  setTelno("")
  	    setEmail("")
        setUsername("")
        setPassword("")
        setRoleId("")
        setImageUrl("")
    }
	}

     // uploading image
    const [loading, setLoading] = useState(false);
    const [imageUrl,setImageUrl] = useState();

    function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
      }


      function beforeUpload(file) {
          const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
          if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
          }
          const isLt2M = file.size / 1024 / 1024 < 2;
          if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
          }
          return isJpgOrPng && isLt2M;
        }


  const handleChange = info => {
    if (info.file.status === 'uploading') {
      setLoading(true)
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>{
          setImageUrl(imageUrl)
          setLoading(false)
      }
      );
    }


  };

    const uploadButton = (
      <div>
        <Icon type={loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );


      


    return(
        <div>

      <Form layout="vertical" onSubmit={updateUser}>
        <Form.Item label="First Name">
          <Input onChange={firstnameChangeHandler} value={firstname}/>
        </Form.Item>
        <Form.Item label="Last Name">
          <Input onChange={lastnameChangeHandler} value={lastname}/>
        </Form.Item>
        <Form.Item label="Address">
          <Input onChange={addressChangeHandler} value={address}  />
        </Form.Item>
        <Form.Item label="Tel No">
          <Input onChange={telnoChangeHandler} value={telno}/>
        </Form.Item>
        <Form.Item label="E-mail">
          <Input onChange={emailChangeHandler} value={email}/>
        </Form.Item>
        <Form.Item label="User Name">
          <Input onChange={usernameChangeHandler} value={username}/>
        </Form.Item>
        <Form.Item label="Password">
          <Input onChange={passwordChangeHandler} value={password} type="password"/>
        </Form.Item>
        <Form.Item label="Role">
        <Select  onChange={roleIdChangeHandler} value={roleId}>
        {
		      	rolesData.map(role=>{
		      		return(
		      			<Option value={role.id} selected={role.id === roleId?true:false}>{role.name}</Option>
		      			);
		      	})
		      }
        </Select>
        </Form.Item>
        <Form.Item>
            <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            beforeUpload={beforeUpload}
            onChange={handleChange}
          >
            {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
          </Upload>
        </Form.Item>
        <Form.Item>
          <Button type="primary"  htmlType="submit" block>
            Update
          </Button>
        </Form.Item>
      </Form>

        </div>
    );
};


export default compose(
	
    graphql(getRolesQuery,{name:"getRolesQuery"}),
    graphql(updateUserMutation,{name:"updateUserMutation"}),
    graphql(getUserQuery,{name:"getUserQuery",
    	options:props => {
    		return{
    			variables:{
    				id:props.id
    			}
    		}
    	}

	})

	)(UpdateUser);