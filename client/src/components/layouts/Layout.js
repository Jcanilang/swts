import React from "react";
import "../../App.css";
import {  Layout, Menu, Icon } from 'antd';
import { Link } from "react-router-dom";


const { Sider } = Layout;

const SideBar = (props)=> {


      const logChecker = () =>{

        if(!props.username){
          return(
            <Menu.Item key="5">
              <a href="/">
                <Icon type="login" />
                <span className="nav-text">Login</span>
              </a>
              </Menu.Item>
          );
        }
        else{
          return(
            <Menu.Item key="5">
              <a href="/logout">
                <Icon type="logout" />
                <span className="nav-text">Logout</span>
              </a>
              </Menu.Item>
          );
        }
      }
 
    return(
        <div>
            <Sider
          className="Admin-sider"
          breakpoint="lg"
          collapsedWidth="0"
          onBreakpoint={broken => {
            console.log(broken);
          }}
          onCollapse={(collapsed, type) => {
            console.log(collapsed, type);
          }}
        >
      <div className="logo">
        <Icon type="home" id="logo" />
        <h2>SWTS</h2>

    
      </div>
      <Menu theme="dark" mode="inline">
        <Menu.Item>
        Hi! {props.username ? props.username : "Guest"}
        </Menu.Item>
        <Menu.Item key="1">
          <a href="/admin">
          <Icon type="dashboard" />
          <span className="nav-text">Dashboard</span>
          </a>
        </Menu.Item>

        <Menu.Item key="2">
          <a href="/admin/users">
          <Icon type="user" />
          <span className="nav-text">User</span>
          </a>
        </Menu.Item>
      
        
        <Menu.Item key="3">
          <a href="/admin/huts">
          <Icon type="home" />
          <span className="nav-text">Hut</span>
          </a>
        </Menu.Item>

        <Menu.Item key="4">
        <a href="/admin/bookings">
          <Icon type="book" />
          <span className="nav-text">Booking</span>
          </a>
        </Menu.Item>
        
        {logChecker()}
       
      </Menu>
    </Sider>
        </div>
    );
  
};

export default SideBar;

