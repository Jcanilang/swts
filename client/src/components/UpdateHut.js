import React,{useState, useEffect} from "react";
import "../App.css";
import {Link} from "react-router-dom";
import {graphql} from "react-apollo";
import {flowRight as compose} from "lodash";
import {
    Form,
    Input,
    Icon,
    Cascader,
    Select,
    Row,
    Col,
    Checkbox,
    Button,
    Table, 
    Divider,
    Modal,
    message,
    Upload
  } from 'antd';

  import {getHutQuery,getCategoriesQuery,getHutStatusesQuery} from "../queries/queries";
  import { updateHutMutation } from "../queries/mutations"; 


  const { Option } = Select;
   // const { confirm } = Modal;

const UpdateHut = (props)=> {
	// console.log(props.id)
    // const mongoose = require('mongoose');
	const dataCategories = props.getCategoriesQuery;
	const dataHutStatuses = props.getHutStatusesQuery;

	let hut = props.getHutQuery.getHut? props.getHutQuery.getHut :{};
	const categoriesData = dataCategories.getCategories ? dataCategories.getCategories:[];
	const hutStatusesData = dataHutStatuses.getHutStatuses ? dataHutStatuses.getHutStatuses:[];

	// console.log(hutsData);
	// hooks
	const [id, setId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [category, setCategory] = useState("");
	const [status, setStatus] = useState("");

	// useEffect(()=>{
	// 	console.log(id);
	// 	console.log(name);
	// 	console.log(description);
	// 	console.log(price);
	// 	console.log(category);
	// 	console.log(status);
	// });

	// console.log(hut);

	if(!props.getHutQuery.loading){
		const setDefaultHut = ()=>{
			setId(hut.id)
			setName(hut.name);
			setDescription(hut.description);
			setPrice(hut.price);
			setCategory(hut.categoryId);
			setStatus(hut.hutStatusId);
		}
		if(id === ""){
			setDefaultHut();
		}
	}

	const nameChangeHandler = e =>{
		setName(e.target.value);
	}

	const descriptionChangeHandler = e =>{
		setDescription(e.target.value);
	}

	const priceChangeHandler = e =>{
		setPrice(e.target.value);
	}

	const categoryChangeHandler = e =>{
		setCategory(e);
	}

	const statusChangeHandler = e =>{
        setStatus(e);
        // console.log(e)
	}

	


	const updateHut = e =>{

		e.preventDefault();
    if(imageUrl == null || name == "" || description== "" || price== "" || status== "" || category== ""){
      // imagePath = nodeServer()+"/images/b857ab90-1b48-11ea-8070-a3e900f687b3.png";
      message.error("Please fill up all the field!!");
    }
    else{

		let updateHutValue = {
			id: hut.id,
			name: name,
		    description: description,
		    price: parseFloat(price),
		    hutStatusId: status,
		    categoryId: category,
        	image: imageUrl
		}

		let secondsToGo = 1;
	    const modal = Modal.success({
	        title: 'Update Success!',
	        content: `This modal will be destroyed after ${secondsToGo} second.`,
	        render: window.location.href ="/admin/huts"
	    });
	    const timer = setInterval(() => {
	        secondsToGo -= 1;
	        modal.update({
	        content: `This modal will be destroyed after ${secondsToGo} second.`,
	        });
	    }, 1000);
	    setTimeout(() => {
	        clearInterval(timer);

	        modal.destroy();
	        
	    }, secondsToGo * 1000)

			props.updateHutMutation({
				variables:updateHutValue
				
			})


        console.log("success");
        
        setDescription("");
        setCategory("");
        setName("");
        setPrice("");
        setStatus("")
        setImageUrl("")

    }
	}

	     // uploading image
    const [loading, setLoading] = useState(false);
    const [imageUrl,setImageUrl] = useState();

    function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
      }


      function beforeUpload(file) {
          const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
          if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
          }
          const isLt2M = file.size / 1024 / 1024 < 2;
          if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
          }
          return isJpgOrPng && isLt2M;
        }


  const handleChange = info => {
    if (info.file.status === 'uploading') {
      setLoading(true)
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>{
          setImageUrl(imageUrl)
          setLoading(false)
      }
      );
    }


  };

    const uploadButton = (
      <div>
        <Icon type={loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
      


    return(
        <div>

        
      <Form onSubmit={updateHut}>
        <Form.Item label="Hut Name">
          <Input onChange={nameChangeHandler} value={name}/>
        </Form.Item>
        <Form.Item label="Price">
          <Input type="Number" onChange={priceChangeHandler} value={price}/>
        </Form.Item>
        <Form.Item label="Status">
        <Select  onChange={statusChangeHandler} value={status}>
       
        {
		      	hutStatusesData.map(hutstatus=>{
		      		return(
		      			<Option value={hutstatus.id} selected={hutstatus.id === status? true:false}>{hutstatus.name}</Option>
		      			);
		      	})
		      }
        </Select>
        </Form.Item>
        <Form.Item label="Category">
        <Select onChange={categoryChangeHandler} value={category} >
       
        {
		      	categoriesData.map(category=>{
		      		return(
          <Option value={category.id} selected={category.id === category?true:false}>{category.name}</Option>
          );
		    })
		}
        </Select>
        </Form.Item>
        <Form.Item label="Description">
          <Input.TextArea rows={4} onChange={descriptionChangeHandler} value={description}  />
        </Form.Item>
        <Form.Item>
            <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            beforeUpload={beforeUpload}
            onChange={handleChange}
          >
            {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
          </Upload>
        </Form.Item>
        <Form.Item>
          <Button type="primary"  htmlType="submit" block>
           Update
          </Button>
        </Form.Item>
      </Form>

        </div>
    );
};


export default compose(
	
	graphql(getCategoriesQuery,{name:"getCategoriesQuery"}),
    graphql(getHutStatusesQuery,{name:"getHutStatusesQuery"}),
    graphql(updateHutMutation,{name:"updateHutMutation"}),
    graphql(getHutQuery,{name:"getHutQuery",
    	options:props => {
    		return{
    			variables:{
    				id:props.id
    			}
    		}
    	}

	})

	)(UpdateHut);